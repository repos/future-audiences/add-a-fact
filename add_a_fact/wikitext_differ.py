import asyncio
import httpx
from html_diff import diff
from pydantic import BaseModel

class HtmlDiffResult(BaseModel):
    diff: str
    old_html: str
    new_html: str

async def diff_to_html(article: str, old_wikitext: str, new_wikitext: str) -> str:
    # first render the two pieces of wikitext
    async with httpx.AsyncClient() as client:
        BASE_URL = f"https://en.wikipedia.org/w/rest.php/v1/transform/wikitext/to/html/{article}"
        old = client.post(BASE_URL, json={"wikitext": old_wikitext})
        new = client.post(BASE_URL, json={"wikitext": new_wikitext})
    
        old, new = await asyncio.gather(old, new)
        old.raise_for_status()
        new.raise_for_status()

        old_html = old.text
        new_html = new.text

        # now diff them
        return HtmlDiffResult(diff=diff(old_html, new_html), old_html=old_html, new_html=new_html)