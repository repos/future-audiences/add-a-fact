import json
from groq import AsyncGroq

SYSTEM = """
You are acting as a researcher for Wikipedia in 2025. You will be given a true or false claim which will be inserted into an article.
Your job is to find the section in the given Wikipedia article where the claim should be inserted, if any.

Your input format will be:

  Article: "John Smith"
  Claim: "John Smith died at home in 1995"
  Sections: ["Abstract", "Early Life", "Career", "Personal Life", "Later life"]

Your output should be a JSON blob in the format:
    {"section": "$SECTION_NAME"}

The first section is always called the "Abstract" and is a summary of the article that contains key facts about the subject. Major information about the subject of the article should be in the "Abstract" section. Some short articles have most of their freetext content in the Abstract and everything goes in there.
"""


class SectionPicker:
    """
    When presented a series of sections and a claim it identifies which section the claim should be inserted into (if any).
    """
    def __init__(self, llm_service: AsyncGroq, model: str | None = 'llama3-70b-8192'):
        self._llm_service = llm_service
        self._model = model

    async def pick_section(self, claim: str, sections: list[str], article: str) -> str | None:
        chat_completion = await self._llm_service.chat.completions.create(
            messages=[
                {
                    "role": "system",
                    "content": SYSTEM,
                },
                {
                    "role": "user",
                    "content": "Article: " + article
                },
                {
                    "role": "user",
                    "content": "Claim: " + claim
                },
                {
                    "role": "user",
                    "content": "Sections: " + json.dumps(sections)
                }
            ],
            response_format={ "type": "json_object" },
            model=self._model,
            temperature=0.0,
        )
        res = json.loads(chat_completion.choices[0].message.content)
        if 'section' in res:
            return res['section']
        else:
            return None