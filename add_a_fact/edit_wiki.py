

from groq import AsyncGroq
import httpx
from pydantic import BaseModel
from add_a_fact.section_picker import SectionPicker
from wme.on_demand import Filter, OnDemand, EnterpriseAPIResponse, ArticleSections

SYSTEM = """
You are acting as a researcher for Wikipedia in 2025. You will be given a true or false claim and a section from Wikipedia where the claim might belong.
Your job is to insert the claim into the section of the article. Only change text that you need to in order to make the claim fit into the text. 
You should add a reference to the claim in the text by placing the magic $REF string after your changed text. Do not insert your own generated references. Only use the magic string.

If you are providing a statistic that changes over time specify the point in time the statistic was valid. For example "As of 2023, the population of the city was 100,000".

Your input format will be:
    Claim: "John Smith died at home in 1995"
    Article: '''John Smith''' (born 11 April 1903 in [[Dillenburg]], [[Germany]]) is a German entomologist and teacher.

Your output should be the new text of the article section with the claim inserted in the correct section. Example:

    '''John Smith''' (born 11 April 1903 in [[Dillenburg]], [[Germany]], , died 1995) was a German entomologist and teacher. John Smith died at his home in 1995. $REF

Only output the wikitext of the section given. Do not output anything else or commentary.

"""

class EditResult(BaseModel):
    old_section_text: str
    section_text: str
    section_idx: int

def build_toc(sections: list[ArticleSections]) -> list[str]:
    out = []
    for section in sections:
        if section.name:
            out.append(section.name)
        if section.has_parts:
            subsections = build_toc([ArticleSections(**p) for p in section.has_parts])
            prepended_subsections = [section.name + " / " + sub for sub in subsections]
            out.extend(prepended_subsections)
    return out

class EditWiki():
    def __init__(self, llm_service: AsyncGroq,  wme_token: str, model: str = "llama3-70b-8192", wiki: str="enwiki"):
        self._wme_token = wme_token
        self._on_demand = OnDemand(wme_token)
        self._wiki = wiki
        self._llm_service = llm_service
        self._model = model

    async def edit(self, fact: str, article: str) -> EditResult:
        """
        insert fact into article
        """
        wiki_filter = Filter.for_site("enwiki")
        articles = await self._on_demand.lookup_structured(article, limit=1, fields=["article_sections"], filters=[wiki_filter])
        if not articles:
            raise Exception("Article missing!")
        article_content = articles[0]
        sections = build_toc(article_content.article_sections)

        sp = SectionPicker(self._llm_service)
        section = await sp.pick_section(fact, sections, article)
        if not section:
            raise Exception("No section found!")
        
        try:
            section_idx = sections.index(section)
        except ValueError:
            raise Exception("Section not found in article!")
        
        # now get the wikitext of the section we want to edit
        URL = f"https://en.wikipedia.org/w/api.php?action=query&prop=revisions&titles={article}&format=json&rvslots=*&rvprop=content&rvsection={section_idx}"
        response = httpx.get(URL)
        response.raise_for_status()
        data = response.json()
        pg = data['query']['pages']
        page = pg[next(iter(pg))]
        revision = page['revisions'][0]
        content = revision['slots']['main']['*']

        chat_completion = await self._llm_service.chat.completions.create(
            messages=[
                {
                    "role": "system",
                    "content": SYSTEM,
                },
                {
                    "role": "user",
                    "content": "Claim: " + fact
                },
                {
                    "role": "user",
                    "content": "Article: " + content
                }                
            ],
            model=self._model,
            temperature=0.0,
        )

        return EditResult(old_section_text=content, section_text=chat_completion.choices[0].message.content, section_idx=section_idx)