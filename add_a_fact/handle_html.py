
from bs4 import BeautifulSoup
import trafilatura
import re

def html_to_plaintext(html: str) -> str:
    elem = trafilatura.load_html(html)
    extraction = trafilatura.bare_extraction(elem)

    title = extraction['title']
    body = extraction['raw_text']
    author = extraction['author']
    date = extraction['date']
    sitename = extraction['sitename']

    return f"""
    Title: {title}
    Author: {author}
    Date: {date}
    Site: {sitename}

    {body}
    """


