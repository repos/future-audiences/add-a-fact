from groq import AsyncGroq
import json
from pydantic import BaseModel
from typing import List

SYSTEM = """
You will be given a claim that is true or false.
You may also be provided some contextual information to help you understand but you are only making keywords for the given claim.
You job is to find information to support or refute this claim on Wikipedia.
To do this you will provide:
  - a search term to search Wikipedia with
  - a list of keywords to find the section within the found article
Output your answer as a JSON in the form: {"search_term": "wikipedia search term", "keywords": ["keyword1", "keyword2"]}

Examples: 
  Claim: Rishi Sunak announces UK general election for Thursday 4 July 2024
  Output: {"search_term": "UK general election July", "keywords": ["Rishi Sunak", "Thursday 4 July"]}

  Claim: Former Republican presidential candidate Nikki Haley has said she plans to vote for Donald Trump, her former opponent and boss, in the 2024 US presidential election.
  Output: {"search_term": "Nikki Haley Donald Trump 2024", "keywords": ["Republican presidential candidate", "vote", "2024 US presidential election"]}

  Claim: In 1985, the number of grizzly bears in the Yellowstone region was as low as 200. By 2010, this number had risen to 600.
  Output: {"search_term": "Grizzly Bear Yellowstone", "keywords": ["1985", "2010", "number of bears"]}

  Claim: The San José was carrying an immense bounty of gold, silver and emeralds from Latin America back to Spain in 1708 when it was sunk by a British fleet off the coast of Cartagena.
  Output: {"search_term": "San Jose ship 1708", "keywords": ["sunk", "shipwreck", "cartagena", "gold", "emeralds"]}

  Claim: On Sunday night, when Cloudflare CEO Matthew Prince announced in a blog post that the company was terminating service for 8chan, the response was nearly universal: Finally.
  Output: {"search_term": "8chan", "keywords": ["Cloudflare", "Matthew Prince", "terminate"]}
"""


class SearchTerm(BaseModel):
    search_term: str
    keywords: List[str]


class KeywordGenerator:
    def __init__(self, llm_service: AsyncGroq, model: str | None = 'llama3-70b-8192'):
        self._llm_service = llm_service
        self._model = model

    async def generate_keywords(self, claim, context="") -> SearchTerm:
        chat_completion = await self._llm_service.chat.completions.create(
            messages=[
                {
                    "role": "system",
                    "content": SYSTEM,
                },
                {
                    "role": "user",
                    "content": "Context: " + context
                },
                {
                    "role": "user",
                    "content": "Claim: " + claim
                }
            ],
            response_format={ "type": "json_object" },
            model=self._model,
            temperature=0.0,
        )

        return SearchTerm(**json.loads(chat_completion.choices[0].message.content))