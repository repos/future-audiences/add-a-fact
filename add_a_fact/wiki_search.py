import httpx
from pydantic import BaseModel

class WikiResult(BaseModel):
    ns: int
    title: str
    pageid: int
    size: int
    wordcount: int
    snippet: str
    timestamp: str

def search_wiki(query: str, limit: int = 3, lang: str = "en") -> list[WikiResult]:
    url = f'https://{lang}.wikipedia.org/w/api.php'
    params = {
        'action': 'query',
        'format': 'json',
        'list': 'search',
        'srsearch': query,
        'srlimit': limit,
        'srnamespace': 0
    }
    response = httpx.get(url, params=params)
    response.raise_for_status()
    return [WikiResult(**result) for result in response.json()['query']['search']]

if __name__ == '__main__':
    query = input('Search query: ')
    results = search_wiki(query)
    for result in results:
        print(result)