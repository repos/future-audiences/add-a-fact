import json
from groq import AsyncGroq

SYSTEM = """
You are acting as a researcher for Wikipedia. You will be given a true or false claim and your job is to identify the article the claim would belong in. 
You will also be given a list of article titles to choose from.

Your input format will be:

  Claim: "John Smith died at home in 1995"
  Articles: ["Blacksmiths", "John Smith", "Rocket Scientists", "Home Deaths", "Austrian People"]

Your output should be a JSON blob in the format:
    {"article": "John Smith"}

Or if no article seems like a good fit then return
    {"article": null}

You may also be provided some contextual information to help you understand but you are only making a decision on which article the claim belongs in.
"""


class ArticlePicker:
    """
    When presented a series of article titles and a claim it identifies which article the claim should be inserted into (if any).
    """
    def __init__(self, llm_service: AsyncGroq, model: str | None = 'llama3-70b-8192'):
        self._llm_service = llm_service
        self._model = model

    async def pick_article(self, claim: str, articles: list[str], context: str="") -> str | None:
        chat_completion = await self._llm_service.chat.completions.create(
            messages=[
                {
                    "role": "system",
                    "content": SYSTEM,
                },
                {
                    "role": "user",
                    "content": "Context: " + context
                },
                {
                    "role": "user",
                    "content": "Claim: " + claim
                },
                {
                    "role": "user",
                    "content": "Articles: " + json.dumps(articles)
                }

            ],
            response_format={ "type": "json_object" },
            model=self._model,
            temperature=0.0,
        )

        picked_article = json.loads(chat_completion.choices[0].message.content)['article']
        if picked_article not in articles:
            return None

        return picked_article