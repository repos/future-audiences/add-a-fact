
from typing import Any
from groq import AsyncGroq
from pydantic import BaseModel
from add_a_fact.wiki_search import search_wiki
from add_a_fact.keyword_generator import KeywordGenerator, SearchTerm
from add_a_fact.entailment import EntailmentDetector, EntailmentResult
from wme.on_demand import ArticleSections, OnDemand, Filter

IGNORED_SECTIONS = set(["gallery", "references", "external links", "further reading", "see also", "notes", "additional sources", "sources", "bibliography"])
NUM_SECTIONS = 10

def score_toc(sections: list[ArticleSections], st: SearchTerm):
    score = 0
    for section in sections:
        if (section.name or "").lower() in IGNORED_SECTIONS:
            continue
        if section.value:
            score = section_match_score(section.value, st)
            yield (section, score)
        if section.has_parts:
            yield from score_toc(list(section), st)


def section_match_score(text: str, st: SearchTerm):
    score = 0
    text = text.lower()
    for kw in st.keywords + [st.search_term]:
        if kw in text:
            score += 2
        kw_bits = kw.split()
        for kw_bit in kw_bits:
            if kw_bit in text:
                score += 1/(len(kw_bits))
    return score

class VerificationtResult(BaseModel):
    sections: list[Any]
    search_term: SearchTerm
    result: EntailmentResult
    read_articles: list[str]


class VerifyClaim:
    def __init__(self, llm_service: AsyncGroq, wme_token: str, model: str | None = 'llama3-70b-8192'):
        self._kw_gen = KeywordGenerator(llm_service, model)
        self._entailment = EntailmentDetector(llm_service, model)
        self._wme_token = wme_token
        self._on_demand = OnDemand(wme_token)

    async def verify(self, claim: str, context="") -> VerificationtResult:
        kw = await self._kw_gen.generate_keywords(claim, context=context)
        results = search_wiki(kw.search_term)

        scored_sections = []
        for result in results:
            fields = ['article_sections']
            filters = [Filter.for_site("enwiki")]
            articles =  await self._on_demand.lookup_structured(result.title, limit=1, fields=fields, filters=filters)
            if not articles:
                raise Exception("Article missing!")
            
            sections = articles[0].article_sections
            scored_sections.extend([(score, section, result) for (section, score) in score_toc(sections, kw)])
        scored_sections.sort(key=lambda x: x[0], reverse=True)

        top_sections = scored_sections[:NUM_SECTIONS]
        read_articles = list(set([r.title for _, _, r in top_sections]))
        
        passages = []
        for _, section, result in top_sections:
            passages.append(f"{result}: {section.value}")
        result = await self._entailment.detect(claim, passages)
        return VerificationtResult(sections=top_sections, result=result, search_term=kw, read_articles=read_articles)

                

