from groq import AsyncGroq
import json


SYSTEM = """
You will be given an article on some subject. Your job is to extract key facts from the article.
These facts should be appropriate for inclusion into Wikipedia. Produce only up to 5 facts. Each fact should be a short sentence. Start with the most consequential facts. Do not directly quote the source or use text verbatim. Summarize the key details and drop minor details.

Do not use pronouns or ambiguous references in any output fact. Be specific. For example, instead of "He was a great leader", say "Napoleon was a great leader". Instead of "the city" say the name of the city. 
Each fact should be understandable on its own. Restate the proper nouns in each fact.

If you are providing a statistic that changes over time specify the point in time the statistic was valid. For example "As of 2023, the population of the city was 100,000".

Your output should be a JSON object. The object should have a key called "facts" which is an array of strings. Each element of that array should be a standalone fact that is true by itself.

Example:

Input
-----
Article: Back in November 1872, for instance, the newspaper publisher and Democratic presidential candidate Horace Greeley died after Election Day but before the casting of Electoral College votes. While it did not affect the outcome – President Ulysses S. Grant easily won reelection – Greeley’s death created the difficult question of what to do with the 66 Electoral College votes he had won. Most electors, meeting in state capitols, did not cast votes for the deceased Greeley, but rather split them among four other candidates. Congress did not count the three votes that were cast for a dead man. In the more than 150 years since Greeley’s death, there have been two constitutional amendments related to presidential succession, but there is still some gray area when it comes to an unforeseen event that strikes a presidential nominee or candidate.


Output
------
{"facts": ["Presidential candidate Horace Greeley died between election day and the casting of electoral votes", "Horace Greeley's 66 electoral votes were split between other candidates", "There is still uncertainty about what happens if a presidential candidate dies"]}

"""



class Extractor:
    def __init__(self, llm_service: AsyncGroq, model: str | None = 'llama3-70b-8192'):
        self._llm_service = llm_service
        self._model = model

    async def extract_claim(self, text) -> list[str]:
        chat_completion = await self._llm_service.chat.completions.create(
            messages=[
                {
                    "role": "system",
                    "content": SYSTEM,
                },
                {
                    "role": "user",
                    "content": "Article: " + text
                }
            ],
            response_format={ "type": "json_object" },
            model=self._model,
            temperature=0.0,
        )

        return json.loads(chat_completion.choices[0].message.content)['facts']