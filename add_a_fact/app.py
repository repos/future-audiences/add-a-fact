import contextlib
from datetime import datetime
from datetime import timedelta
from fastapi import Body, Depends, FastAPI, Request
from groq import AsyncGroq
from typing import Annotated, AsyncIterator, TypedDict
from pydantic import BaseModel
from pydantic_settings import BaseSettings, SettingsConfigDict
import asyncio

import httpx
import uvicorn

from add_a_fact.article_picker import ArticlePicker
from add_a_fact.edit_wiki import EditResult, EditWiki
from add_a_fact.extract_claims import Extractor
from add_a_fact.handle_html import html_to_plaintext
from add_a_fact.reference_generator import generate_reference_wikitext
from add_a_fact.verify_claim import VerificationtResult, VerifyClaim
from add_a_fact.wiki_search import search_wiki
from wme.auth import refresh_token, TokenResponse, login
from fastapi import HTTPException


from add_a_fact.wikitext_differ import HtmlDiffResult, diff_to_html
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from pathlib import Path

class AuthState(TypedDict):
    refreshing_token: str | None
    access_token: str | None

class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env', env_file_encoding='utf-8')
    wme_key: str | None = None # legacy
    wme_username: str
    wme_password: str
    open_api_key: str | None = None
    groq_api_key: str | None = None

BASE_PATH = Path(__file__).resolve().parent
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))

TOKEN_REFRESH_BUFFER_SEC = 60 * 60 # 1 hour buffer for token refresh
settings = Settings()
auth_state: AuthState = { "refreshing_token": None, "access_token": None}

@contextlib.asynccontextmanager
async def lifespan(app):
    # grab a refresh token
    creds = await login(settings.wme_username, settings.wme_password)
    expires_in_seconds = creds.expires_in - TOKEN_REFRESH_BUFFER_SEC
    # compute datetime we are expired at
    expired_at = datetime.now() + timedelta(seconds=expires_in_seconds)
    auth_state["refreshing_token"] =  creds.refresh_token
    auth_state["access_token"] = creds.access_token
    auth_state["token_expired_at"] = expired_at
    yield


app = FastAPI(lifespan=lifespan)

headers={"User-Agent": "Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148"}

class AddAFactResponse(BaseModel):
    comment: str
    claim: str
    html_diff: HtmlDiffResult | None = None
    edit_to_article: str | None = None
    edit: EditResult | None
    verification: VerificationtResult | None
    context: str | None = None


async def wme_auth(request: Request) -> str:
    expires_at = auth_state['token_expired_at']
    if expires_at < datetime.now():
        print("refreshing token!")
        token_response = await refresh_token(settings.wme_username, auth_state["access_token"])
        auth_state['access_token'] = token_response.access_token
        auth_state['token_expired_at'] = datetime.now() + timedelta(seconds=token_response.expires_in - TOKEN_REFRESH_BUFFER_SEC)
        return token_response.access_token
    else:
        return auth_state['access_token']
    
WmeAuthDep = Annotated[str, Depends(wme_auth)]


async def make_edit(client: AsyncGroq, url:str, html:str, wme_auth: str, claim: str, context: str) -> AddAFactResponse:
    vc = VerifyClaim(client, wme_auth)
    verification_result = await vc.verify(claim, context=context)
    verification_result.sections = []
    if verification_result.result.result != "unclear":
        # skip results where we already have the truth
        return AddAFactResponse(comment=f"skipped - claim is {verification_result.result.result}", claim=claim, verification=verification_result, edit=None, context=context)

    titles = [r.title for r in search_wiki(verification_result.search_term.search_term, limit=20)]
    ap = ArticlePicker(client)
    article = await ap.pick_article(claim, titles, context=context)

    if not article:
        return AddAFactResponse(comment="no article found to insert into", claim=claim, verification=verification_result, edit=None, context=context)

    ew = EditWiki(client, wme_auth)
    edit = await ew.edit(claim, article)

    reference = generate_reference_wikitext(url, html)
    new_text = edit.section_text.replace("$REF", reference)
    edit.section_text = new_text

    html_diff = await diff_to_html(article, edit.old_section_text, edit.section_text)

    return AddAFactResponse(comment="success!", html_diff=html_diff, claim=claim, verification=verification_result, edit_to_article=article, edit=edit, context=context)

@app.get("/add_a_fact_html")
async def add_a_fact_html(request: Request, url: str, wme_auth: WmeAuthDep) -> HTMLResponse:
    responses = await add_a_fact(url, wme_auth)
    return templates.TemplateResponse(
        request=request, name="edits.html", context={"url": url, "edits": responses}
    )

@app.get("/add_a_fact")
async def add_a_fact(url: str, wme_auth: WmeAuthDep) -> list[AddAFactResponse]:
    client = AsyncGroq(api_key=settings.groq_api_key)
    html = httpx.get(url, headers=headers).text
    article_text = html_to_plaintext(html)

    ex = Extractor(client)
    claims = await ex.extract_claim(article_text)

    claim_results = []
    for idx, claim in enumerate(claims):
        context = "\n".join(claims[0:idx])
        claim_results.append(make_edit(client, url, html, wme_auth, claim, context))
    claim_results = await asyncio.gather(*claim_results)
    return claim_results


@app.post("/add_a_fact")
async def add_a_fact_post(html: Annotated[str, Body()], url: str, wme_auth: WmeAuthDep) -> list[AddAFactResponse]:
    client = AsyncGroq(api_key=settings.groq_api_key)
    article_text = html_to_plaintext(html)
    ex = Extractor(client)
    claims = await ex.extract_claim(article_text)
    claim_results = []
    for idx, claim in enumerate(claims):
        context = "\n".join(claims[0:idx])
        claim_results.append(make_edit(client, url, html, wme_auth, claim, context))
    claim_results = await asyncio.gather(*claim_results)
    return claim_results

def run():
    uvicorn.run(app, host="0.0.0.0", port=3001)

if __name__ == "__main__":
    run()