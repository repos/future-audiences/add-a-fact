from groq import AsyncGroq
import json
from pydantic import BaseModel
from typing import List

SYSTEM = """
You will be given a true or false claim and list of text passages from Wikipedia relevant to that claim.

Based on the provided sources, make a decision on whether the claim is 'correct', 'incorrect', 'partially_correct' or 'unclear'.
Then select a short, VERBATIM, quote from one of the text passages that validates or rejects the claim.

If the claim is neither confirmed or denied by the passages given then return "unclear" even if it seems unlikely.

Return your answer in the JSON format: 
  {"selected_quote": "Homeopathy is a pseudoscience, a belief that is incorrectly presented ...", "explanation": "There are matching sources to the claim", "result": "correct"}

If there is no quote in the section and the decision is 'unclear' you can omit the quote as in:
  {"explanation": "The passages don't mention anything about homeopathy", "result": "unclear"}

"""

class EntailmentResult(BaseModel):
    selected_quote: str | None = None
    explanation: str
    result: str


class EntailmentDetector:
    def __init__(self, llm_service: AsyncGroq, model: str | None = 'llama3-70b-8192'):
        self._llm_service = llm_service
        self._model = model

    async def detect(self, claim: str, passages: list[str]) -> EntailmentResult:
        chat_completion = await self._llm_service.chat.completions.create(
            messages=[
                {
                    "role": "system",
                    "content": SYSTEM,
                },
                {
                    "role": "user",
                    "content": "Passages: " + json.dumps(passages)
                },
                {
                    "role": "user",
                    "content": "Claim: " + claim
                }

            ],
            response_format={ "type": "json_object" },
            model=self._model,
            temperature=0.0,
        )

        return EntailmentResult(**json.loads(chat_completion.choices[0].message.content))