from datetime import date

def generate_reference_wikitext(url: str, html: str):
    today = date.today().isoformat()
    return "<ref>{{cite web |url=" + url + " |title= |website= |access-date=" + today + " }}</ref>"